# Changelog
All notable changes to this project will be documented in this file. The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.2] - 2020-05-21
### Added
 - Added Spotify widget for Polybar

### Changed
 - Moved pleroma ZSH theme to .oh-my-zsh/themes directory.

## [1.3.1] - 2020-05-20
### Added
 - Added keybinding to reload Polybar (useful when changing config file).
 - Added ranger_archives plugin to extract/compress archive files from within Ranger.
 - Added scope.sh to Ranger configuration.
 - Added zsh aliases file.
 - Added youtube-dl and mpv to installed packages.
 - Added pleroma colorscheme for ranger.
 - Added nano support to ranger (after highting a text file, just type nano).
 - Added pleroma theme to ZSH to match the desktop (based on Agnoster's Theme).
 - Added Musl/Glibc detection. NVIDIA proprietary drivers and Atom installations are skipped if Musl is detected.

### Changed
 - Changed Polybar glyph icons to size 8 to match text font.
 - Changed Polybar underline color to match other themes.
 - Changed PDF viewer from Evince to XPDF.
 - Changed workspace desktop rules for BSPWM.
 - Renamed "Music" workspace to "Media"
 - Changed ranger config to use new pleroma colorscheme.
 - Updated project screenshot.

## [1.3.0] - 2020-05-19
### Added
 - Added credit to whereismycow42 for the really helpful improvement suggestions.
 - User now has the option to perform or bypass a system upgrade before continuing.
 - User now has the option to install Spicetify and desktop matching theme for Spotify or skip Spicetify installation.
 - Added "shopt -s dotglob" before copying dotfiles to fix "No matches found" error.

### Changed
 - Changed installation instructions in README.
 - Only sync Void repos once at the beginning of the script instead of at every application installation.
 - Changed "Starting post-installation script..." to read "Starting Pleroma..."
 - Moved libGL installation to the NVIDIA driver installation section.
 - Changed sxhkd mute toggle Keybindings.

### Removed
 - Removed sleep timer between functions.
 - Removed "Press ENTER to reboot" and replaced it with note informing user to reboot.
 - Removed "source $HOME/.zshrc" since user is asked to reboot after Pleroma finishes.
 - Removed self-destruction function.

## [1.2.0] - 2020-05-18
### Added
 - Added detection for NVIDIA graphics cards. If an NVIDIA card is detected, Pleroma will install the proprietary drivers. Otherwise, the script will continue to the next step.
 - Added Spicetify specific settings to zshrc.
 - Added custom Pleroma theme for Spicetify to match the rest of the desktop.

### Changed
 - Refactored how additional applications are installed to prepare Pleroma for user input on whether to install them or not.

### Removed
 - Removed timezone configuration as this is usually done during Void installation.
 - Removed adding user to preset permission groups as this is usually done during Void installation.
 - Removed Pywal and Gruvbox themes for Spicetify.

## [1.1.0] - 2020-05-17
### Added
 - Added LICENSE
 - Added user input on whether to continue running the script after launch.
 - Added sudo check in case user has not specified running as sudo.
 - Reload the ZSH configuration after importing dot files.
 - Install Flathub repos during system configuration (no software is installed).
 - Added list of installed applications to README.
 - Added Spicetify installation for Spotify theming.

### Changed
 - Updated README to include specifying sudo when running script.
 - Fixed bug where LibreOffice would not open in the specified workspace.

## [1.0.0] - 2020-05-17
### Added
- Initial source release.
